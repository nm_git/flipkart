package com.jspiders.leaveapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jspiders.leaveapp.entity.LeaveDetails;
import com.jspiders.leaveapp.responsedto.ResponseDto;
import com.jspiders.leaveapp.service.LeaveService;

@RestController
@RequestMapping("/leaveController")
public class LeaveController {
	@Autowired
	private LeaveService leaveService;

	@PostMapping("/saveLeaveDetails")
	public @ResponseBody ResponseDto saveLeaveDetails(@RequestBody LeaveDetails leaveDetails) {
		System.out.println(this.getClass().getSimpleName());
		return leaveService.processLeaveDetails(leaveDetails);
	}

}
