package com.jspiders.leaveapp.responsedto;

import java.io.Serializable;

public class ResponseDto implements Serializable {
	private String statusCode;
	private String status;
	private Object data;
	private String errorMessage;

	public ResponseDto(String statusCode, String status, Object data, String errorMessage) {
		super();
		this.statusCode = statusCode;
		this.status = status;
		this.data = data;
		this.errorMessage = errorMessage;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	@Override
	public String toString() {
		return "ResponseDto [statusCode=" + statusCode + ", status=" + status + ", data=" + data + ", errorMessage="
				+ errorMessage + ", getStatusCode()=" + getStatusCode() + ", getStatus()=" + getStatus()
				+ ", getData()=" + getData() + ", getErrorMessage()=" + getErrorMessage() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

}
