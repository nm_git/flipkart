package com.jspiders.leaveapp.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.jspiders.leaveapp.constants.LeaveConstants.AppConstants;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = AppConstants.LEAVE_INFO)
public class LeaveDetails implements Serializable {

	@Id
	@GenericGenerator(name = "auto", strategy = "increment")
	@GeneratedValue(generator = "auto")
	@Column(name = "id")
	private long id;

	@Column(name = "employee_id")
	private int employeeId;

	@Column(name = "leave_start_date")
	private String leaveStartDate;

	@Column(name = "leave_end_date")
	private String leaveEndDate;

	@Column(name = "number_of_days")
	private String numberOfDays;

}
