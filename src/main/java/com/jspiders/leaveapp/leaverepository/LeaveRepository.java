package com.jspiders.leaveapp.leaverepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jspiders.leaveapp.entity.LeaveDetails;


@Repository
public interface LeaveRepository extends JpaRepository<LeaveDetails, Long> {

}
