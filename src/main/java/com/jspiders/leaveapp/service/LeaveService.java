package com.jspiders.leaveapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jspiders.leaveapp.entity.LeaveDetails;
import com.jspiders.leaveapp.leaverepository.LeaveRepository;
import com.jspiders.leaveapp.responsedto.ResponseDto;

@Service
public class LeaveService {

	@Autowired
	private LeaveRepository leaveRepository;

	public ResponseDto processLeaveDetails(LeaveDetails leaveDetails) {
		try {

			LeaveDetails details = leaveRepository.save(leaveDetails);
			//test request
			System.out.println(getClass().getSimpleName());
			return new ResponseDto("200", "Success", details, null);

		} catch (Exception e) {
			return new ResponseDto("500", "Failure", null, e.getLocalizedMessage());
		}

	}

}
